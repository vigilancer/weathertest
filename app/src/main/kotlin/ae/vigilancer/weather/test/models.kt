package ae.vigilancer.weather.test

import android.database.Cursor
import com.google.auto.value.AutoValue
import com.squareup.sqldelight.RowMapper
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

@AutoValue
abstract class CityDbRow : CityModel {
    class Marshal : CityModel.CityMarshal<Marshal>()

    companion object {
        @JvmField val MAPPER: CityModel.Mapper<CityDbRow> = CityModel.Mapper(
                CityModel.Mapper.Creator<CityDbRow>
                { _id, name, latitude, longitude, user_defined ->
                    AutoValue_CityDbRow(_id, name, latitude, longitude, user_defined) })
    }
}

@AutoValue
abstract class WeatherDbRow : WeatherModel {
    class Marshal : WeatherModel.WeatherMarshal<Marshal>()

    companion object {
        @JvmField val MAPPER: WeatherModel.Mapper<WeatherDbRow> = WeatherModel.Mapper(
                WeatherModel.Mapper.Creator<WeatherDbRow>
                { _id, time_sec_gmt, temp_c, city_id ->
                    AutoValue_WeatherDbRow(_id, time_sec_gmt, temp_c, city_id) })
    }
}

@AutoValue
abstract class WeatherCityDbRow : WeatherCityModel {
    companion object {
        @JvmField val MAPPER: WeatherCityModel.Mapper<WeatherCityDbRow> = WeatherCityModel.Mapper(
            object : WeatherCityModel.Mapper.Companion.Creator<WeatherCityDbRow> {
                override fun create(city_id: Long, city_name: String, time_sec_gmt: Long, temperature_c: Double): WeatherCityDbRow {
                    return AutoValue_WeatherCityDbRow(city_id, city_name, time_sec_gmt, temperature_c)
                }
        } )
    }
}

// custom sql requests
interface WeatherCityModel {
    fun city_id(): Long
    fun city_name(): String
    fun time_sec_gmt(): Long
    fun temperature_c(): Double

    class Mapper<T : WeatherCityModel> : RowMapper<T> {
        private lateinit var creator: Mapper.Companion.Creator<T>

        constructor(creator: Creator<T>) {
            this.creator = creator
        }

        override fun map(cursor: Cursor): T {
            return creator.create(
                cursor.getLong(cursor.getColumnIndex("city_id")),
                cursor.getString(cursor.getColumnIndex("city_name")),
                cursor.getLong(cursor.getColumnIndex("time_sec_gmt")),
                cursor.getDouble(cursor.getColumnIndex("temperature_c"))
            );
        }

        companion object {
            interface Creator<R : WeatherCityModel> {
                fun create(city_id: Long, city_name: String, time_sec_gmt: Long, temperature_c: Double): R
            }
        }
    }
}

data class Weather(val temperature: String, val summary: String, val date: LocalDate) {
    companion object {
        @JvmStatic fun from(w: WeatherCityDbRow): Weather {
            return Weather(
                "${w.temperature_c().toString().replace('.', ',')} °C",
                "",
                LocalDate.from(ZonedDateTime.ofInstant(Instant.ofEpochMilli(w.time_sec_gmt() * 1000), ZoneId.systemDefault()))
            )
        }
    }
}
data class City(val name: String, val current: Weather, val forecast: List<Weather>?)
