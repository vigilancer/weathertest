package ae.vigilancer.weather.test

import ae.vigilancer.weather.test.data.DbOpenHelper
import android.app.Application
import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import com.squareup.sqlbrite.BriteDatabase
import com.squareup.sqlbrite.SqlBrite
import rx.schedulers.Schedulers
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var bdb: BriteDatabase
        lateinit var dbh: DbOpenHelper
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        dbh = DbOpenHelper(this)
        val sqlBrite = SqlBrite.create()
        bdb = sqlBrite.wrapDatabaseHelper(dbh, Schedulers.io())

        initDebug()
    }

    private fun initDebug() {
        if (!BuildConfig.DEBUG) return

        Stetho.initializeWithDefaults(this)
        Timber.plant(Timber.DebugTree())
//        LeakCanary.install(this)
    }
}