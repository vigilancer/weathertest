package ae.vigilancer.weather.test

import ae.vigilancer.weather.test.network.sync.sync.SyncAdapter
import ae.vigilancer.weather.test.ui.CityAdapter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.util.*

class MainActivity : AppCompatActivity() {
    private val db = App.bdb
    private var weatherSubscription: Subscription? = null
    private val catchDbUpdates = db.createQuery("weather", WeatherModel.SELECT_ALL_WITH_CITY) // подписываемся на события изменения базы
        .flatMap { q -> q.asRows { c -> WeatherCityDbRow.MAPPER.map(c) }.toList() } // запрашиваем изменения
    private lateinit var adapter: CityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar_actionbar) as Toolbar)
        adapter = CityAdapter()

        val lm = LinearLayoutManager(this)
        lm.orientation = LinearLayoutManager.VERTICAL
        city_list.setHasFixedSize(true)
        city_list.layoutManager = lm
        city_list.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        if (weatherSubscription != null && !weatherSubscription!!.isUnsubscribed)
            return

        weatherSubscription = catchDbUpdates
            .map { weathers_db ->
                val cities = weathers_db.groupBy { w -> w.city_id() }
                val ret = ArrayList<City>()
                cities.forEach { w ->
                    val current: WeatherCityDbRow = w.value.filter { w ->
                        val forecast_date = ZonedDateTime.ofInstant(Instant.ofEpochMilli(w.time_sec_gmt() * 1000), ZoneId.systemDefault())
                        val now = LocalDate.now()
                        now.isEqual(LocalDate.from(forecast_date))
                    }[0]
                    val forecasts = w.value.filter { it.time_sec_gmt() != current.time_sec_gmt() }.sortedBy { it.time_sec_gmt() }
                        .map { w -> Weather.from(w) }
                    ret.add(City(current.city_name(), Weather.from(current), forecasts))
                }
                ret
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ cities ->
                Timber.d("catch update")
                Timber.d("${cities.size}. $cities")
                adapter.update(cities)
            },
                { e -> Timber.e("something wrong. ", e) }
            )
    }

    override fun onPause() {
        weatherSubscription?.unsubscribe()
        weatherSubscription = null
        super.onPause()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_action_refresh) {
            SyncAdapter.requestSync(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
}
