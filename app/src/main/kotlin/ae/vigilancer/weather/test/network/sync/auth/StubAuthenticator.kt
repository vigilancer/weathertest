package ae.vigilancer.weather.test.network.sync.auth

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.content.Context
import android.os.Bundle

/**
 * Stub implementation
 */

class StubAuthenticator(context: Context) : AbstractAccountAuthenticator(context) {

    // Getting a label for the auth token is not supported
    override fun getAuthTokenLabel(authTokenType: String?): String? {
        throw UnsupportedOperationException()
    }

    // Ignore attempts to confirm credentials
    override fun confirmCredentials(response: AccountAuthenticatorResponse?, account: Account?, options: Bundle?): Bundle? {
        return null
    }

    // Updating user credentials is not supported
    override fun updateCredentials(response: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, options: Bundle?): Bundle? {
        throw UnsupportedOperationException()
    }

    // Getting an authentication token is not supported
    override fun getAuthToken(response: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, options: Bundle?): Bundle? {
        throw UnsupportedOperationException()
    }

    // Checking features for the account is not supported
    override fun hasFeatures(response: AccountAuthenticatorResponse?, account: Account?, features: Array<out String>?): Bundle? {
        throw UnsupportedOperationException()
    }

    // Editing properties is not supported
    override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?): Bundle? {
        throw UnsupportedOperationException()
    }

    // Don't add additional accounts
    override fun addAccount(response: AccountAuthenticatorResponse?, accountType: String?, authTokenType: String?, requiredFeatures: Array<out String>?, options: Bundle?): Bundle? {
        return null
    }
}