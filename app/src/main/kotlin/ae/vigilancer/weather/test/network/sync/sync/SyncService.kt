package ae.vigilancer.weather.test.network.sync.sync

import android.app.Service
import android.content.Intent
import android.os.IBinder

class SyncService : Service() {

    companion object {
        private var syncAdapter: SyncAdapter? = null
        private val syncAdapterLock = Object()
    }

    override fun onCreate() {
        synchronized(syncAdapterLock) {
            if (syncAdapter == null)
                syncAdapter = SyncAdapter(applicationContext, true)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return syncAdapter?.syncAdapterBinder
    }
}