package ae.vigilancer.weather.test.network.sync.sync

import ae.vigilancer.weather.test.App
import ae.vigilancer.weather.test.CityDbRow
import ae.vigilancer.weather.test.CityModel
import ae.vigilancer.weather.test.WeatherDbRow
import ae.vigilancer.weather.test.network.service
import android.accounts.Account
import android.accounts.AccountManager
import android.content.*
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.sqlbrite.BriteDatabase
import rx.Observable
import timber.log.Timber

class SyncAdapter(context: Context?, autoInitialize: Boolean)
        : AbstractThreadedSyncAdapter(context, autoInitialize, false) {

    companion object {
        private const val DAY_SEC = 24 * 60 * 60    // 24 hours
        private const val PERIODIC_SYNC_INTERVAL = 60L * 60  // 60 mins

        private val AUTHORITY = "ae.vigilancer.weather.test.provider"
        private val ACCOUNT_TYPE = "ae.vigilancer.weather.test.account";
        private val ACCOUNT = "Weather Account";

        @SuppressWarnings("NewApi")     // get rid of false positives
        fun requestSync(context: Context) {
            val args = Bundle()
            args.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true)
            args.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true)

            val dummyAccount = Account(ACCOUNT, ACCOUNT_TYPE)
            val accountManager = context.applicationContext.getSystemService(AppCompatActivity.ACCOUNT_SERVICE) as AccountManager
            accountManager.addAccountExplicitly(dummyAccount, null, null)

            // schedule periodic sync
            ContentResolver.addPeriodicSync(
                Account(ACCOUNT, ACCOUNT_TYPE),
                AUTHORITY,
                Bundle.EMPTY,
                PERIODIC_SYNC_INTERVAL)

            ContentResolver.setIsSyncable(dummyAccount, AUTHORITY, 1)
            ContentResolver.requestSync(dummyAccount, AUTHORITY, args)
        }
    }

    override fun onPerformSync(
            account: Account?,
            extras: Bundle?,
            authority: String?,
            provider: ContentProviderClient?,
            syncResult: SyncResult?)
    {
        val cur_time_gmt = System.currentTimeMillis()/1000

        val db = App.bdb
        var t: BriteDatabase.Transaction? = null

        getCities(db)
            .take(1)    // just in case
            .subscribe(
                { list ->
                    // транзакцию можно начать только после того, как получим все города
                    Observable.from(list)
                        // в одну транзакцию обновляем погоду во всех городах
                        .doOnSubscribe { t = db.newTransaction() }
                        .doOnCompleted { t!!.markSuccessful() }
                        .doAfterTerminate { t!!.end() }
                        .subscribe(
                            { city ->
                                Observable.range(0, 4)  // today, +1 day, +2 days,...
                                    /* delete all relevant weather for city */
                                    .doOnSubscribe { db.delete("weather", "city_id=?", city._id().toString()) }
                                    .flatMap { day_offset ->
                                        service.getSingle(city.latitude(), city.longitude(), cur_time_gmt + day_offset * DAY_SEC) }
                                    .subscribe(
                                        { forecast ->
                                            /* fill 4-days weather */
                                            val w = WeatherDbRow.Marshal()
                                                    .city_id(city._id())
                                                    .temperature_c(forecast.currently.temperature)
                                                    .time_sec_gmt(forecast.currently.time)
                                                    .asContentValues()
                                            db.insert("weather", w)
                                            Timber.v("sync weather: $w")
                                        }
                                    )
                            }
                        )
                }
            )
    }

    private fun getCities(db: BriteDatabase): Observable<List<CityDbRow>> {
        return db.createQuery("city", CityModel.SELECT_ALL).mapToList{ c -> CityDbRow.MAPPER.map(c) }
    }

}