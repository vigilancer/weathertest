package ae.vigilancer.weather.test.network.sync.auth

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri

class StubContentProvider : ContentProvider() {

    // Always return true, indicating that the provider loaded correctly.
    override fun onCreate(): Boolean {
        return true
    }

    // insert() always returns null (no URI)
    override fun insert(uri: Uri?, values: ContentValues?): Uri? {
        throw UnsupportedOperationException()
    }

    // query() always returns no results
    override fun query(uri: Uri?, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
        throw UnsupportedOperationException()
    }

    // update() always returns "no rows affected" (0)
    override fun update(uri: Uri?, values: ContentValues?, selection: String?, selectionArgs: Array<out String>?): Int {
        throw UnsupportedOperationException()
    }

    // delete() always returns "no rows affected" (0)
    override fun delete(uri: Uri?, selection: String?, selectionArgs: Array<out String>?): Int {
        throw UnsupportedOperationException()
    }

    // Return no type for MIME type
    override fun getType(uri: Uri?): String? {
        throw UnsupportedOperationException()
    }
}