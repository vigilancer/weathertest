package ae.vigilancer.weather.test.data

import ae.vigilancer.weather.test.CityDbRow
import ae.vigilancer.weather.test.CityModel
import ae.vigilancer.weather.test.WeatherModel
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbOpenHelper(context: Context)
        : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "weather_test.db"
        private const val DATABASE_VERSION = 1;
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CityModel.CREATE_TABLE)
        db.execSQL(WeatherModel.CREATE_TABLE)

        //populate initial data
        db.insert(CityModel.TABLE_NAME, null,
                CityDbRow.Marshal()
                        .name("Москва")
                        .user_defined(false)
                        .latitude(55.7522200)
                        .longitude(37.6155600)
                        .asContentValues())
        db.insert(CityModel.TABLE_NAME, null,
                CityDbRow.Marshal()
                        .name("Питер")
                        .user_defined(false)
                        .latitude(59.9386300)
                        .longitude(30.3141300)
                        .asContentValues())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}
