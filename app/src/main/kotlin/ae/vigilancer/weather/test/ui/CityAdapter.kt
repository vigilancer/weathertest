package ae.vigilancer.weather.test.ui

import ae.vigilancer.weather.test.City
import ae.vigilancer.weather.test.R
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.util.*

class CityAdapter : RecyclerView.Adapter<CityAdapter.VH>() {
    private val cities: ArrayList<City> = ArrayList(3)

    companion object {
        private const val layoutId = R.layout.item_city
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(cities[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return VH(v)
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    fun update(weather: List<City>) {
        Timber.d("updating adapter: $weather")
        cities.clear()
        cities.addAll(weather)
        notifyDataSetChanged()
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name: TextView by lazy { itemView.findViewById(R.id.city_name) as TextView }
        private val temp: TextView by lazy { itemView.findViewById(R.id.city_temp) as TextView }
        private val summary: TextView by lazy { itemView.findViewById(R.id.city_summary) as TextView }
        private val forecast1: TextView by lazy { itemView.findViewById(R.id.forecast_1) as TextView }
        private val forecast2: TextView by lazy { itemView.findViewById(R.id.forecast_2) as TextView }
        private val forecast3: TextView by lazy { itemView.findViewById(R.id.forecast_3) as TextView }

        fun bind(city: City) {
            name.text = city.name
            summary.text = city.current.date.format(DateTimeFormatter.ISO_LOCAL_DATE)
            temp.text = city.current.temperature

            city.forecast?.let { f ->
                f.getOrNull(0)?.let { forecast1.text = it.temperature }
                f.getOrNull(1)?.let { forecast2.text = it.temperature }
                f.getOrNull(2)?.let { forecast3.text = it.temperature }
            }
        }
    }
}