package ae.vigilancer.weather.test.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import rx.Observable

interface Forecast {
    @GET("/forecast/$FORECAST_IO_KEY/{lat},{long},{time}?units=si&exclude=minutely,hourly,daily,alerts,flags&lang=ru")
    fun getSingle(@Path("lat") lat: Double, @Path("long") long: Double, @Path("time") time_sec_gmt: Long)
            : Observable<SingleForecastResponse>

}

const val FORECAST_IO_KEY = "daa615c6d4c92ca986bbd2d084ac2cdf"
const val FORECAST_IO_ENDPOINT = "https://api.forecast.io/"

val gson = Gson()
val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
val okhttp3 = OkHttpClient.Builder().addInterceptor(logging).addNetworkInterceptor(StethoInterceptor()).build()

val retrofit = Retrofit.Builder()
                .baseUrl(FORECAST_IO_ENDPOINT)
                .client(okhttp3)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()

val service = retrofit.create(Forecast::class.java)



data class Currently(val time: Long, val temperature: Double, val summary: String?)
data class DailyDataItem(val time: Long, val temperatureMin: Double, val temperatureMax: Double)
data class Daily(val data: Array<DailyDataItem>)
//data class ForecastResponse(val latitude: Double, val longitude: Double, val timezone: String,
//                            val currently: Currently, val daily: Daily)

data class SingleForecastResponse(val latitude: Double, val longitude: Double, val timezone: String,
                            val currently: Currently)
